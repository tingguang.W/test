//  g++ -o transport transport.cpp -llapack  -lblas
#include <iostream>
#include <complex>
#include <vector>
#include <cblas.h>

extern "C"{
    // LAPACK function for QR decomposition
    void zgeqrf_(int* m, int* n, std::complex<double>* a, int* lda, std::complex<double>* tau,
                 std::complex<double>* work, int* lwork, int* info);

    // LAPACK function to generate Q matrix from outputs of zgeqrf
    void zungqr_(int* m, int* n, int* k, std::complex<double>* a, int* lda, std::complex<double>* tau,
                 std::complex<double>* work, int* lwork, int* info);
       // LAPACK function to computes an LU factorization of a general M-by-N matrix A
    void zgetrf_(int* m, int* n, std::complex<double>* A, int* lda, int* ipiv, int* info);
    // LAPACK function to computes the inverse of a matrix using the LU factorization computed by zgetrf.
    void zgetri_(int* n, std::complex<double>* A, int* lda, int* ipiv,
     std::complex<double>* work, int* lwork, int* info);
}
    // calculate inverse of mat,mat=Inverse(mat),m-by-m
void Inverse(std::complex<double>* mat, int m);
void HermiConj(std::complex<double>* mat, int row, int col);
void Transport(const std::complex<double> *X,int m ,int n, std::complex<double> *R, std::complex<double> *Z);

int main() {
    int m = 6; // Number of rows of A
    int n = 2; // Number of columns of A (m >= n)
    // Example matrix A
    std::complex<double> X[m*n] = {
        {-1.800000, -0.900000},{-1.600000,  1.000000},
        {-1.000000, -0.300000},{ 1.100000, -0.100000},
        {0.500000,  0.700000},{-1.500000, -0.700000}, 
        { 1.100000, -0.800000},{ 1.700000,  1.400000},
        {1.200000,  0.300000},{0.700000, -1.900000},
        {-0.200000, -1.500000},{ 1.800000, -0.600000}

    };
    std::complex<double> Z[m*n] ={
   {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
   {0.0318 , 0.7952 },   {0.3171 , 0.7547 },
   {0.2769 , 0.1869 },   {0.9502 , 0.2760 },
   {0.0462 , 0.4898 },   {0.0344 , 0.6797 },
   {0.0971 , 0.4456 },   {0.4387 , 0.6551 },
   { 0.8235 , 0.6463},   {0.3816 , 0.1626 }
    };
    std::complex<double> alpha{1,0};
    std::complex<double> beta{0,0};
    std::complex<double> R[n*n]={
    {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
    {0.0318 , 0.7952 },   {0.3171 , 0.7547 }
    };
    Transport(X, m, n,R,Z);
               for (int i =0; i< m*n; ++i)
    {
        std::cout << Z[i] << "  ";
        if ((i+1)%n == 0)
        {
            std::cout<< std::endl;
        }
    }
    return 0;
}

void Transport(const std::complex<double> *X, int m ,int n, std::complex<double> *R, std::complex<double> *Z)
{
    std::complex<double> alpha{1,0};
    std::complex<double> beta{0,0};
   std::complex<double>* tempmn = new std::complex<double>[m*n];
    std::complex<double>* tempmn1 = new std::complex<double>[m*n];
    std::complex<double>* tempnn= new std::complex<double>[n*n];
    std::complex<double>* tempnn1= new std::complex<double>[n*n];
    std::complex<double> alpha1 = std::complex<double>{-1,0};
    cblas_zcopy(n*n,R,1,tempnn,1);
    Inverse(tempnn, n); // R^-1
    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, n, &alpha, Z, n, tempnn, n, &beta, tempmn, n); // tempmn=Z*R^-1
    cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, n, n, m, &alpha, X, n, tempmn, n, &beta, tempnn1, n); // tempnn1=(x^+)*tempmn
    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, n, &alpha, X, n, tempnn1, n, &beta, tempmn1, n); // tempmn1=X*tempnn1
    cblas_zaxpy(m*n, &alpha1, tempmn1, 1, tempmn, 1); // tempmn = alpha1*tempmn1+tempmn
    for (int i=0; i<n; ++i){
        for(int j=0; j<n; ++j){
            if (i >= j){
                tempnn1[i*n+j] = beta;
            }
        }
    }
    cblas_zcopy(n*n,tempnn1,1,tempnn,1);
    HermiConj(tempnn1, n, n);
    cblas_zaxpy(n*n, &alpha1, tempnn1, 1, tempnn, 1);
    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, n, &alpha, X, n, tempnn, n, &beta, Z, n);
    cblas_zaxpy(m*n, &alpha, tempmn, 1, Z, 1);
    delete[] tempmn;
    delete[] tempmn1;
    delete[] tempnn;
    delete[] tempnn1;
}

void HermiConj(std::complex<double>* mat, int row, int col){
        std::complex<double>* result = new std::complex<double>[row*col];
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                result[j * row + i] = std::conj(mat[i * col + j]);  // Conjugate and transpose
            }
        }
        cblas_zcopy(row*col,result,1,mat,1);
        delete[] result;
    }
void Inverse(std::complex<double>* mat, int m) {
        int lda = m;
        int* ipiv = new int[m];
        int info;

        zgetrf_(&m, &m, mat, &lda, ipiv, &info); // LU decomposition
        if (info != 0) {
            std::cerr << "LU decomposition failed" << std::endl;
            delete[] ipiv;
            return;
        }

        int lwork = m;
        std::complex<double>* work = new std::complex<double>[lwork];
        zgetri_(&m, mat, &lda, ipiv, work, &lwork, &info); // calculate inverse of matrix
        if (info != 0) {
            std::cerr << "Matrix inversion failed" << std::endl;
            delete[] ipiv;
            delete[] work;
            return;
        }

        delete[] ipiv;
        delete[] work;
    }
