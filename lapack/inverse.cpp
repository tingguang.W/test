//  g++ -o inv inverse.cpp -llapack  -lblas
#include <iostream>
#include <complex>

extern "C" {
    // LAPACK 函数声明
    void zgetrf_(int* M, int* N, std::complex<double>* A, int* lda, int* ipiv, int* info);
    void zgetri_(int* N, std::complex<double>* A, int* lda, int* ipiv, std::complex<double>* work, int* lwork, int* info);
}

void printMatrix(const std::complex<double>* matrix, int rows, int cols) {
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            std::cout << matrix[i * cols + j].real() << " + " << matrix[i * cols + j].imag() << "i\t";
        }
        std::cout << std::endl;
    }
}

void inverseMatrix(std::complex<double>* matrix, int N) {
    int lda = N;
    int* ipiv = new int[N];
    int info;

    zgetrf_(&N, &N, matrix, &lda, ipiv, &info); // LU 分解
    if (info != 0) {
        std::cerr << "LU decomposition failed" << std::endl;
        delete[] ipiv;
        return;
    }

    int lwork = N;
    std::complex<double>* work = new std::complex<double>[lwork];
    zgetri_(&N, matrix, &lda, ipiv, work, &lwork, &info); // 计算逆矩阵
    if (info != 0) {
        std::cerr << "Matrix inversion failed" << std::endl;
        delete[] ipiv;
        delete[] work;
        return;
    }

    delete[] ipiv;
    delete[] work;
}

int main() {
    const int N = 5;
    std::complex<double> matrix[N*N] = { 
 {0.2399 , 0.3532}, {0.0497 , 0.6491}, {0.3377 , 0.2963}, {0.3897 , 0.3685}, {0.9421 , 0.7757},
 {0.1233 , 0.8212}, {0.9027 , 0.7317}, {0.9001 , 0.7447}, {0.2417 , 0.6256}, {0.9561 , 0.4868},
 {0.1839 , 0.0154}, {0.9448 , 0.6477}, {0.3692 , 0.1890}, {0.4039 , 0.7802}, {0.5752 , 0.4359},
 {0.2400 , 0.0430}, {0.4909 , 0.4509}, {0.1112 , 0.6868}, {0.0965 , 0.0811}, {0.0598 , 0.4468},
 {0.4173 , 0.1690}, {0.4893 , 0.5470}, {0.7803 , 0.1835}, {0.1320 , 0.9294}, {0.2348 , 0.3063}
};

    std::cout << "Original matrix:\n";
    printMatrix(reinterpret_cast<std::complex<double>*>(matrix), N, N);

    // 求逆
    inverseMatrix(reinterpret_cast<std::complex<double>*>(matrix), N);

    std::cout << "Inverse matrix:\n";
    printMatrix(reinterpret_cast<std::complex<double>*>(matrix), N, N);

    return 0;
}
