//  g++ -o norm norm.cpp -llapack  -lblas
#include <iostream>
#include <complex>

extern "C" {
    // LAPACK 函数原型声明the value of the one norm,  or the Frobenius norm, or
    // the  infinity norm ...
    extern double zlange_(char* norm, int* m, int* n, std::complex<double>* A, int* lda);
}

void printMatrix(const std::complex<double>* matrix, int rows, int cols) {
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            std::cout << matrix[i * cols + j].real() << " + " << matrix[i * cols + j].imag() << "i\t";
        }
        std::cout << std::endl;
    }
}

int main() {
    int m = 6; // Number of rows of A
    int n = 2; // Number of columns of A (m >= n)
    // Example matrix AX
    std::complex<double> X1[m*n] = {
        {-1.800000, -0.900000},{-1.600000,  1.000000},
        {-1.000000, -0.300000},{ 1.100000, -0.100000},
        {0.500000,  0.700000},{-1.500000, -0.700000}, 
        { 1.100000, -0.800000},{ 1.700000,  1.400000},
        {1.200000,  0.300000},{0.700000, -1.900000},
        {-0.200000, -1.500000},{ 1.800000, -0.600000}

    };

    int lda = m; // 矩阵A的第一个维度
    char norm = 'F'; // 范数类型为Frobenius范数

    // 计算矩阵A的Frobenius范数
    double real_norm = zlange_(&norm, &m, &n, X1, &lda);

    // 输出结果
    std::cout << "Frobenius norm of matrix A: " << real_norm << std::endl;
    return 0;
}
