//  g++ -o QR QR.cpp -llapack  -lblas
#include <iostream>
#include <complex>
#include <vector>
#include <cblas.h>

extern "C"{
    // LAPACK function for QR decomposition
    void zgeqrf_(int* m, int* n, std::complex<double>* a, int* lda, std::complex<double>* tau,
                 std::complex<double>* work, int* lwork, int* info);

    // LAPACK function to generate Q matrix from outputs of zgeqrf
    void zungqr_(int* m, int* n, int* k, std::complex<double>* a, int* lda, std::complex<double>* tau,
                 std::complex<double>* work, int* lwork, int* info);
}
void Retraction( std::complex<double> *Z,int m,int n,std::complex<double> tau,std::complex<double> *X,std::complex<double> *R);
void TransX(std::complex<double>* X, const int row, const int col);
void Qr(const std::complex<double> *X,int m, int n, std::complex<double> *Q, std::complex<double> *R);
int main() {
    int m = 6; // Number of rows of A
    int n = 2; // Number of columns of A (m >= n)
    // Example matrix A
    std::complex<double> A[m*n] = {
        {-1.800000, -0.900000},{-1.600000,  1.000000},
        {-1.000000, -0.300000},{ 1.100000, -0.100000},
        {0.500000,  0.700000},{-1.500000, -0.700000}, 
        { 1.100000, -0.800000},{ 1.700000,  1.400000},
        {1.200000,  0.300000},{0.700000, -1.900000},
        {-0.200000, -1.500000},{ 1.800000, -0.600000}

    };
    std::complex<double> Z[m*n] ={
   {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
   {0.0318 , 0.7952 },   {0.3171 , 0.7547 },
   {0.2769 , 0.1869 },   {0.9502 , 0.2760 },
   {0.0462 , 0.4898 },   {0.0344 , 0.6797 },
   {0.0971 , 0.4456 },   {0.4387 , 0.6551 },
   { 0.8235 , 0.6463},   {0.3816 , 0.1626 }

    };
    std::complex<double> alpha{1,0};
    std::complex<double> beta{0,0};
    std::complex<double>* R = new std::complex<double>[n*n];
    std::complex<double> tau=std::complex<double>{2,1};
    Retraction(Z,m,n,tau,A,R);
    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, n, &alpha, A, n, R, n, &beta, Z, n);
    std::cout<<"QR:"<< std::endl;
    for (int i =0; i< m*n; ++i)
    {   
        std::cout << Z[i] << "  ";
        if ((i+1)%n == 0)
        {
            std::cout<< std::endl;
        }
    }
    std::cout<<"R:"<< std::endl;
    for (int i =0; i< n*n; ++i)
    {   
        std::cout << R[i] << "  ";
        if ((i+1)%n == 0)
        {
            std::cout<< std::endl;
        }
    }
        delete[] R;
    return 0;
}

 void Qr(const std::complex<double> *X,int m, int n, std::complex<double> *Q, std::complex<double> *R)
{
    std::complex<double>* temp_X = new std::complex<double>[m * n];
    // copy X to temp_X
    // std::copy(X, X + m * n, temp_X);
    cblas_zcopy(m*n,X,1,temp_X,1);
    TransX(temp_X, m, n); // converte row major order matrix to column major order
    // initial function's parameters of LAPACK
    int lda = m;
    int info;
    std::complex<double> work_size;
    int lwork = -1;
    std::complex<double>* tau=new std::complex<double>[n];
    // Query the required work size
    zgeqrf_(&m, &n, temp_X, &lda, tau, &work_size, &lwork, &info);
    // Gets the size of the work
    lwork =static_cast<int>(work_size.real());
    std::complex<double>* work = new std::complex<double>[lwork];
    // QR decomposition
    zgeqrf_(&m, &n, temp_X, &lda, tau, work, &lwork, &info);
    if (info != 0) {
        std::cerr << "Error in QR factorization." << std::endl;
        delete[] temp_X;
        delete[] work;
        delete[] tau;
        return;
    }

    // Gets R
    for (int i = 0; i < n; ++i) {  // column
        for (int j = 0; j < n; ++j) { // row
            if (i >= j) {
                R[i * n + j] = temp_X[i* m + j];
            } else {
                R[i * n + j] = 0.0;
            }
        }
    }
    // calculate Q 
    zungqr_(&m, &n, &n, temp_X, &lda, tau, work, &lwork, &info);
    if (info != 0) {
        std::cerr << "Error in computing Q matrix." << std::endl;
        delete[] temp_X;
        delete[] work;
        delete[] tau;
        return;
    }
    // copy temp_X to Q 
    cblas_zcopy(m*n,temp_X,1,Q,1);
    TransX(Q, n, m); // converte column major order matrix to row major order
    TransX(R, n, n); // converte column major order matrix to row major order
    delete[] work;
    delete[] tau;   
    delete[] temp_X;
    return;    
}

void Retraction( std::complex<double> *Z,int m,int n,std::complex<double> tau,std::complex<double> *X,std::complex<double> *R)
{    std::complex<double> alpha{1,0};
    std::complex<double> beta{0,0};
    std::complex<double>* In = new std::complex<double>[n*n];
    std::complex<double>* r = new std::complex<double>[n*n];
    std::complex<double> *q=new std::complex<double>[m*n];
    cblas_zaxpy(m*n, &tau, Z, 1, X, 1); //X=tau*Z+X 
    Qr(X, m, n, q, r);
    
    for (int i = 0; i < n; ++i){
        for (int j = 0; j <n; ++j){
            if (i == j) {
                if (r[i*n+j].real() >=0 ){
                    In[i*n + j] = std::complex<double>(1.0, 0.0);
                } 
                else {
                    In[i*n + j] = std::complex<double>(-1.0, 0.0);
                }  
            }
            else {
                In[i*n + j] = std::complex<double>(0.0, 0.0);
            }
        }
    }
    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, n, &alpha, q, n, In, n, &beta, X, n);
    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, &alpha, In, n, r, n, &beta, R, n);
//  function [q,r]=qr_decom(Y)
// %qr decomposition, q in the Stiefel manifold,
// % Y is a search direction in the tangent space T_X St(n,p)
// [q,r]=qr(Y,0);
// q=q*diag(sign(sign(diag(r))+.5));
// r=diag(sign(sign(diag(r))+.5))*r;
// end;
delete[] q;
delete[] r;
delete[] In;
}

void TransX(std::complex<double>* X, const int row, const int col)
{
        std::complex<double>* temp_X = new std::complex<double>[row * col];
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                temp_X[j *row  + i] = X[ i*col  + j];
            }
        }
        cblas_zcopy(row*col,temp_X,1,X,1);
        delete[] temp_X;
}
