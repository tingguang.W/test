// g++ -o test main.cpp  -lblas
//C=alpha*AB+beta*C
// cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans,
//    m, n, k, alpha, A, m, B, k, beta, C, m);
#include <iostream>
#include <cblas.h>
#include <complex>
#include <vector>
#include <cstring> 
// #include <mkl.h>
void print_matrix(std::complex<double>* mat, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            std::cout << mat[i * cols + j] << " ";
        }
        std::cout << std::endl;
    }
}

int main()
{   
int m = 6; // Number of rows of A
    int n = 2; // Number of columns of A (m >= n)
    // Example matrix A
    std::complex<double> X1[m*n] = {
        {-1.800000, -0.900000},{-1.600000,  1.000000},
        {-1.000000, -0.300000},{ 1.100000, -0.100000},
        {0.500000,  0.700000},{-1.500000, -0.700000}, 
        { 1.100000, -0.800000},{ 1.700000,  1.400000},
        {1.200000,  0.300000},{0.700000, -1.900000},
        {-0.200000, -1.500000},{ 1.800000, -0.600000}

    };
        std::complex<double> X[m*n] = {
        {-1.800000, -0.900000},{-1.600000,  1.000000},
        {-1.000000, -0.300000},{ 1.100000, -0.100000},
        {0.500000,  0.700000},{-1.500000, -0.700000}, 
        { 1.100000, -0.800000},{ 1.700000,  1.400000},
        {1.200000,  0.300000},{0.700000, -1.900000},
        {-0.200000, -1.500000},{ 1.800000, -0.600000}

    };
    std::complex<double> Q[m*n] ={
   {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
   {0.0318 , 0.7952 },   {0.3171 , 0.7547 },
   {0.2769 , 0.1869 },   {0.9502 , 0.2760 },
   {0.0462 , 0.4898 },   {0.0344 , 0.6797 },
   {0.0971 , 0.4456 },   {0.4387 , 0.6551 },
   { 0.8235 , 0.6463},   {0.3816 , 0.1626 }
    };
     std::complex<double> H[m*n] ={
   {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
   {0.0318 , 0.7952 },   {0.3171 , 0.7547 },
   {0.2769 , 0.1869 },   {0.9502 , 0.2760 },
   {0.0462 , 0.4898 },   {0.0344 , 0.6797 },
   {0.0971 , 0.4456 },   {0.4387 , 0.6551 },
   { 0.8235 , 0.6463},   {0.3816 , 0.1626 }
    };
    std::complex<double> alpha{1,0};
    std::complex<double> beta{0,0};
    std::complex<double> R1[n*n]={
    {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
    {0.0318 , 0.7952 },   {0.3171 , 0.7547 }
    };
   std::complex<double> alpha1{-1,0};
          
        std::complex<double> *Mnn = new std::complex<double>[n*n]; // n-by-n
        std::complex<double> M2n2n1[4*n*n] ={
        {-1.800000, -0.900000},{-1.600000,  1.000000},{0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
        {-1.000000, -0.300000},{ 1.100000, -0.100000},{0.0318 , 0.7952 },   {0.3171 , 0.7547 } ,  
        {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },{0.0318 , 0.7952 },   {0.3171 , 0.7547 },
        {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },{0.0318 , 0.7952 },   {0.3171 , 0.7547 } 
        };


        std::complex<double> M2nn [2*n*n]= {
                {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
                {0.0318 , 0.7952 },   {0.3171 , 0.7547 },
                {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
                {0.0318 , 0.7952 },   {0.3171 , 0.7547 }  
        }; // 2n-by-n
        std::complex<double> M2nn1[2*n*n] ={
                {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
                {0.0318 , 0.7952 },   {0.3171 , 0.7547 },
                {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
                {0.0318 , 0.7952 },   {0.3171 , 0.7547 } 
        };
        
        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, 2*n, n, 2*n, &alpha, M2n2n1, 2*n, M2nn, n, &beta, M2nn1, n);
        // [M;N]=M2nn1;
        std::complex<double> *M = new std::complex<double>[n*n]; // n-by-n
        std::complex<double> *N = new std::complex<double>[n*n]; // n-by-n
        std::memcpy(M, M2nn1, n * n * sizeof(std::complex<double>));
        std::memcpy(N, M2nn1 + n * n, n * n * sizeof(std::complex<double>));
        // X=XM+QN
        std::complex<double> *Mmn = new std::complex<double>[m*n]; // m-by-n
        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, n, &alpha, Q, n, N, n, &beta, Mmn, n);
        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, n, &alpha, X, n, M, n, &alpha, Mmn, n);
        cblas_zcopy(m*n,Mmn,1,X,1);
        // tauH=HM-X(R^+)N
        cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, n, n, n, &alpha1, R1, n, N, n, &beta, Mnn, n);

        delete[] N;
        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, n, &alpha, X1, n, Mnn, n, &beta, Mmn, n);
            print_matrix(Mmn, m, n);
        delete[] Mnn;
        cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, n, &alpha, H, n, M, n, &alpha, Mmn, n);
        cblas_zcopy(m*n,Mmn,1,H,1);
        delete[] Mmn;
        delete[] M;
     // Print the resulting matrix B
    std::cout << "Resulting Matrix X:\n";
    print_matrix(X, m, n);
     std::cout << "Resulting Matrix H:\n";
    print_matrix(H, m, n);
   return 0; 
}