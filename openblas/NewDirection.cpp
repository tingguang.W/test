// g++ -o NewDirection  NewDirection.cpp  -lblas
#include <iostream>
#include <cblas.h>
#include <complex>
#include <vector>
// #include <mkl.h>
void print_matrix(std::complex<double>* mat, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            std::cout << mat[i * cols + j] << " ";
        }
        std::cout << std::endl;
    }
}
void NewDirection(const std::complex<double> *G,int m,int n,  const std::complex<double> betak, std::complex<double> *Z);
int main()
{   
     int m = 2;
    int n = 2;

    // Initialize complex matrices A and B
    std::complex<double> G[m*n] = {
        {1.0, 2.0}, {3.0, 4.0},
        {5.0, 6.0}, {7.0, 8.0}
    };
    std::complex<double> Z[] = {
        {8.0, 7.0}, {6.0, 5.0},
        {4.0, 3.0}, {2.0, 1.0}
    };

    // Scalar alpha
    std::complex<double> alpha = {2.0, 0.0};  // Can be any complex number
    NewDirection(G, m, n,alpha, Z);
    // Perform the operation B = alpha * A + B
    // cblas_zaxpy(rows * cols, &alpha, reinterpret_cast<const void*>(A), 1, reinterpret_cast<void*>(B), 1);

    // Print the resulting matrix B
    std::cout << "Resulting Matrix -G+beta*Z:\n";
    print_matrix(Z, m, n);
   return 0; 
}
    //Z=-G+betak*Z
   void NewDirection(const std::complex<double> *G,int m,int n, const std::complex<double> betak, std::complex<double> *Z)
    {   std::complex<double> alpha1{-1,0};
        std::complex<double> *temp=new std::complex<double>[m*n];
        cblas_zaxpy(m*n, &betak, Z, 1, temp, 1); // temp = alphak*Z
        cblas_zaxpy(m*n, &alpha1, G, 1, temp, 1);// temp = -G + temp
        cblas_zcopy(m*n,temp,1,Z,1);
        delete[] temp;
    }