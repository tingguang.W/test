// g++ -o blockmat blockmat.cpp  -lblas
//C=alpha*AB+beta*C
// cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans,
//    m, n, k, alpha, A, m, B, k, beta, C, m);
#include <iostream>
#include <cblas.h>
#include <complex>
#include <cstring> 
    // transposition of matrix, X=X^T
    void Transpose(std::complex<double>* X, const int row, const int col);
    // hermitian conjugate of matrix ,mat=mat^+
    void HermiConj(std::complex<double>* mat, int row, int col);
 void print_matrix(std::complex<double>* mat, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            std::cout << mat[i * cols + j] << " ";
        }
        std::cout << std::endl;
    }
}

int main()
{   
    int m=6;
    int n=2;
    std::complex<double> alpha{1,0};
    std::complex<double> beta{0,0};
    std::complex<double> steplength{2,0};
    std::complex<double> *Mnn = new std::complex<double>[n*n]; // n-by-n
    std::complex<double> *M2nn = new std::complex<double>[2*n*n]; // 2n-by-n
    std::complex<double> *M2n2n = new std::complex<double>[4*n*n]; // 2n-by-2n
    // std::complex<double> tempn[n*n];//={{0,0},{0,0},{0,0},{0,0}};
    std::complex<double> X[2*n*n] = {
        {-1.800000, -0.900000},{-1.600000,  1.000000},
        {-1.000000, -0.300000},{ 1.100000, -0.100000},
        {0.500000,  0.700000},{-1.500000, -0.700000},
        { 1.100000, -0.800000},{ 1.700000,  1.400000}
        // {1.200000,  0.300000},{0.700000, -1.900000},
        // {-0.200000, -1.500000},{ 1.800000, -0.600000}
    };
    // print_matrix(X, n, 2*n);
    std::complex<double> H[2*n*n]={
   {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
   {0.0318 , 0.7952 },   {0.3171 , 0.7547 },
   {0.2769 , 0.1869 },   {0.9502 , 0.2760 },
   {0.0462 , 0.4898 },   {0.0344 , 0.6797 }
//    ,
//    {0.0971 , 0.4456 },   {0.4387 , 0.6551 },
//    { 0.8235 , 0.6463},   {0.3816 , 0.1626 }
    };
    std::complex<double> R[n*n]={
        { -5.5966 , 0.0903},  {-1.8672, - 0.4750},
        { -2.2784, - 1.1558}, {  0.8879, - 0.4408}
        // ,
        // {-0.0147 , 3.0455},  {-0.8206 , 0.5386},
        // { 6.1872, - 2.2428},   {3.4987 , 2.4109},
        // {2.9612, - 0.8316},   {1.9963, - 0.1556},
        // {1.2590 ,- 6.1411},   {3.3999, - 1.9606}
    };

    // Mnn = X'*H;
    cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, n, n, m, &alpha, X, n, H, n, &beta, Mnn, n);
    // Copy Mnn to the upper part of M2nn
    std::memcpy(M2nn, Mnn, n * n * sizeof(std::complex<double>));
    // Copy R to the lower part of M2nn
    std::memcpy(M2nn + n * n, R, n * n * sizeof(std::complex<double>));
    Transpose(M2nn, 2*n, n);
    std::memcpy(M2n2n , M2nn, 2*n * n * sizeof(std::complex<double>));
    HermiConj(R, n, n);
    cblas_zdscal(n*n,-1,R,1); // R=-R
    cblas_zdscal(2*n*n,0,M2nn,1); 
    // Copy -R' to the upper part of M2nn
    std::memcpy(M2nn, R, n * n * sizeof(std::complex<double>));
    // cblas_zdscal(n*n,0,R,1); // R=0*R
    // // Copy 0 to the lower part of M2nn
    // std::memcpy(M2nn + n * n, R, n * n * sizeof(std::complex<double>));
    Transpose(M2nn, 2*n, n);
    std::memcpy(M2n2n +2*n * n, M2nn, 2*n * n * sizeof(std::complex<double>));
    Transpose(M2n2n, 2*n, 2*n);
    // M2n2n = steplength*[Mnn,-R';R,0]
    cblas_zscal(4*n*n,&steplength,M2n2n,1);
    std::cout << "M2n2n" << std::endl;
    print_matrix(M2n2n,2*n,2*n);

    std::complex<double> G1[m*n]={
        { -5.5966 , 0.0903},  {-1.8672, - 0.4750},
        { -2.2784, - 1.1558}, {  0.8879, - 0.4408},
        {-0.0147 , 3.0455},  {-0.8206 , 0.5386},
        { 6.1872, - 2.2428},   {3.4987 , 2.4109},
        {2.9612, - 0.8316},   {1.9963, - 0.1556},
        {1.2590 ,- 6.1411},   {3.3999, - 1.9606}
    };
    // std::complex<double>*  M= new std::complex<double>[m*n];
    std::cout << "" << std::endl;
    // print_matrix(G1,m,n);
    // for (int i =0; i< m*n; ++i)
    // {
    //     std::cout << G[i] << "  ";
    //     if ((i+1)%n == 0)
    //     {
    //         std::cout<< std::endl;
    //     }
    // }
    // delete[] M;
    delete[] Mnn;
    delete[] M2nn;
    delete[] M2n2n;
   return 0; 
}

void Transpose(std::complex<double>* X, const int row, const int col)
    {
        std::complex<double>* temp_X = new std::complex<double>[row * col];
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                temp_X[j *row  + i] = X[ i*col  + j];
            }
        }
        cblas_zcopy(row*col,temp_X,1,X,1);
        delete[] temp_X;
    }

    void HermiConj(std::complex<double>* mat, int row, int col){
        std::complex<double>* result = new std::complex<double>[row*col];
        for (int i = 0; i < row; ++i) {
            for (int j = 0; j < col; ++j) {
                result[j * row + i] = std::conj(mat[i * col + j]);  // Conjugate and transpose
            }
        }
        cblas_zcopy(row*col,result,1,mat,1);
        delete[] result;
    }