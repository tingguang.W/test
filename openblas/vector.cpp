// g++ -o vect vector.cpp  -lblas

#include <iostream>
#include <cblas.h>
#include <complex>
#include <vector>
#include <cstring> 
// #include <mkl.h>
void print_matrix(std::complex<double>* mat, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            std::cout << mat[i * cols + j] << " ";
        }
        std::cout << std::endl;
    }
}

int main()
{
int m = 6; // Number of rows of A
    int n = 2; // Number of columns of A (m >= n)
    // Example matrix A
    //     std::complex<double> X[m*n] = {
    //     {-1.800000, -0.900000},{-1.600000,  1.000000},
    //     {-1.000000, -0.300000},{ 1.100000, -0.100000},
    //     {0.500000,  0.700000},{-1.500000, -0.700000}, 
    //     { 1.100000, -0.800000},{ 1.700000,  1.400000},
    //     {1.200000,  0.300000},{0.700000, -1.900000},
    //     {-0.200000, -1.500000},{ 1.800000, -0.600000}

    // };
        std::complex<double> X[m*n] = {
        {1, 2},{0,  0},
        {0, 0},{ 0, 0000},
        {00,  00000},{000, 0000}, 
        { 00000, 0},{ 0000,000},
        {00,  0},{000, 00000},
        {00000, 00},{ 2, 5}

    };
    std::complex<double> Q[m*n] ={
   {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
   {0.0318 , 0.7952 },   {0.3171 , 0.7547 },
   {0.2769 , 0.1869 },   {0.9502 , 0.2760 },
   {0.0462 , 0.4898 },   {0.0344 , 0.6797 },
   {0.0971 , 0.4456 },   {0.4387 , 0.6551 },
   { 0.8235 , 0.6463},   {0.3816 , 0.1626 }
    };
    std::complex<double> alpha{1,0};
    std::complex<double> beta{0,0};
   std::complex<double> alpha1{-1,0};
    cblas_zaxpy(12,&alpha1,X,1,Q,1);  
        std::complex<double> result0;
       
        std::complex<double>* temp = new std::complex<double>[m*n];
         std::complex<double>* temp0 = new std::complex<double>[m*n];
         cblas_zcopy(m*n,X,1,temp,1);
         cblas_zcopy(m*n,temp,1,temp0,1);
         print_matrix(temp0, m, n);
         result0 = cblas_zdotc(m*n,temp0,1,temp,1);
       // Print the resulting matrix B
     std::cout << result0 <<std::endl;
    std::cout << "Resulting Matrix Q:\n";
    print_matrix(Q, m, n);
    //  std::cout << "Resulting Matrix H:\n";
    // print_matrix(H, m, n);
    delete[] temp;
    delete[] temp0;
   return 0; 
}