//C=alpha*AB+beta*C
// cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans,
//    m, n, k, alpha, A, m, B, k, beta, C, m);
#include <iostream>
#include <cblas.h>
#include <complex>
#include <vector>
void Grad(const std::complex<double> *X,std::complex<double> *G,int m,int n);
void print_matrix(std::complex<double>* mat, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            std::cout << mat[i * cols + j] << " ";
        }
        std::cout << std::endl;
    }
}

int main()
{   
    int m=3;
    int n=2;
    std::complex<double> alpha2{1,0};
    std::complex<double> beta{0,0};
    std::complex<double> tempn[n*n];//={{0,0},{0,0},{0,0},{0,0}};
    std::complex<double> X[m*n]={{1,2},{8,5},{5,1},{4,3},{8,6},{6,2}};
    std::complex<double> G[m*n]={{1,2},{8,5},{9,5},{8,1},{3,5},{3,5}};
    Grad(X,G,m,n);
    for (int i =0; i< m*n; ++i)
    {
        std::cout << G[i] << "  ";
        if ((i+1)%n == 0)
        {
            std::cout<< std::endl;
        }
    }
 
   return 0; 
}

void Grad(const std::complex<double> *X,std::complex<double> *G,int m,int n)
{
    std::complex<double> alpha1{-0.5,0};
    std::complex<double> alpha{1,0};
    std::complex<double> beta{0,0}; 
    std::complex<double> *temp=new std::complex<double>[n*n];
    cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, n, n, m, &alpha1, X, n, G, n, &beta, temp, n);
    cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, n, n, m, &alpha1, G, n, X, n, &alpha, temp, n);
    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, n, &alpha, X, n, temp, n, &alpha, G, n);
    delete[] temp;
}