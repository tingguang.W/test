#include <iostream>
#include <vector>
#include <complex>
    
int main()
{   
    int n=5;
    std::complex<double>* In = new std::complex<double>[n*n];
    for (int i = 0; i < n; ++i)
    {
    for (int j = 0; j <n; ++j) {
        if (i == j) {
            In[i*n + j] = std::complex<double>(1.0, 0.0);
        // } else {
        //     In[i*n + j] = std::complex<double>(0.0, 0.0);
        }
    }
    }
    for (int i =0; i< n*n; ++i)
    {
        std::cout << In[i] << "  ";
        if ((i+1)%n == 0)
        {
            std::cout<< std::endl;
        }
    }
    delete[] In;
    return 0;
}
