// g++ -o innerproduct innerproduct.cpp  -lblas
//C=alpha*AB+beta*C
// cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans,
//    m, n, k, alpha, A, m, B, k, beta, C, m);
#include <iostream>
#include <cblas.h>
#include <complex>
#include <vector>
 std::complex<double> Trace(const std::complex<double> *G1, const std::complex<double> *G2, int row, int col);
std::complex<double> InnerProduct(const std::complex<double> *X, const std::complex<double> *G1, const std::complex<double> *G2, int m,int n);
void print_matrix(std::complex<double>* mat, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            std::cout << mat[i * cols + j] << " ";
        }
        std::cout << std::endl;
    }
}

int main()
{   
    int m=6;
    int n=2;
    // std::complex<double> tempn[n*n];//={{0,0},{0,0},{0,0},{0,0}};
    std::complex<double> X[m*n] = {
        {-1.800000, -0.900000},{-1.600000,  1.000000},
        {-1.000000, -0.300000},{ 1.100000, -0.100000},
        {0.500000,  0.700000},{-1.500000, -0.700000}, 
        { 1.100000, -0.800000},{ 1.700000,  1.400000},
        {1.200000,  0.300000},{0.700000, -1.900000},
        {-0.200000, -1.500000},{ 1.800000, -0.600000}

    };
    std::complex<double> G[m*n]={
   {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
   {0.0318 , 0.7952 },   {0.3171 , 0.7547 },
   {0.2769 , 0.1869 },   {0.9502 , 0.2760 },
   {0.0462 , 0.4898 },   {0.0344 , 0.6797 },
   {0.0971 , 0.4456 },   {0.4387 , 0.6551 },
   { 0.8235 , 0.6463},   {0.3816 , 0.1626 }
    };
    std::complex<double> G1[m*n]={
  { -5.5966 , 0.0903},  {-1.8672, - 0.4750},
 { -2.2784, - 1.1558}, {  0.8879, - 0.4408},
{-0.0147 , 3.0455},  {-0.8206 , 0.5386},
 { 6.1872, - 2.2428},   {3.4987 , 2.4109},
 {2.9612, - 0.8316},   {1.9963, - 0.1556},
  {1.2590 ,- 6.1411},   {3.3999, - 1.9606}
    };


    std::cout << InnerProduct(X,G,G1, m ,n) << std::endl;
    // print_matrix(G,m,n);
    std::cout << Trace(G,G1, m ,n) << std::endl;
    // print_matrix(G1,m,n);
    // for (int i =0; i< m*n; ++i)
    // {
    //     std::cout << G[i] << "  ";
    //     if ((i+1)%n == 0)
    //     {
    //         std::cout<< std::endl;
    //     }
    // }
 
   return 0; 
}

std::complex<double> InnerProduct(const std::complex<double> *X, const std::complex<double> *G1, const std::complex<double> *G2,int m , int n)
{
    std::complex<double> alpha{1,0};
    std::complex<double> beta{0,0};
    std::complex<double> alpha1{-0.5,0};
    // std::complex<double> alpha{1,0};
    std::complex<double> result{0,0}; 
    std::complex<double> *temp=new std::complex<double>[n*n];
    std::complex<double> *tempG=new std::complex<double>[m*n];
    cblas_zcopy(m*n,G2,1,tempG,1);
    cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, n, n, m, &alpha1, X, n, G2, n, &beta, temp, n);
    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, n, &alpha, X, n, temp, n, &alpha, tempG, n);
    result=Trace(G1, tempG, m,n);
    delete[] temp;
    delete[] tempG;
    return result;
}

    std::complex<double> Trace(const std::complex<double> *G1, const std::complex<double> *G2, int row, int col)
    {
        std::complex<double> alpha{1,0};
        std::complex<double> beta{0,0};
        std::complex<double> result{0,0};
        std::complex<double> *temp=new std::complex<double>[col*col];
        cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, col, col, row, &alpha, G1, col, G2, col, &beta, temp, col);
        for(int i=0;i<col;++i)
        {
            result += temp[i*col+i];
        }
        delete[] temp;
        return result;
    }