// g++ -o grad  grad.cpp  -lblas
//C=alpha*AB+beta*C
// cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans,
//    m, n, k, alpha, A, m, B, k, beta, C, m);
#include <iostream>
#include <cblas.h>
#include <complex>
#include <vector>
void Gradqr(const std::complex<double> *X,std::complex<double> *G,int m,int n);
void Gradexp(const std::complex<double> *X,std::complex<double> *G, int m,int n);
void print_matrix(std::complex<double>* mat, int rows, int cols) {
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            std::cout << mat[i * cols + j] << " ";
        }
        std::cout << std::endl;
    }
}

int main()
{   
    int m=6;
    int n=2;
    std::complex<double> alpha2{1,0};
    std::complex<double> beta{0,0};
    std::complex<double> tempn[n*n];//={{0,0},{0,0},{0,0},{0,0}};
    std::complex<double> X[m*n] = {
        {-1.800000, -0.900000},{-1.600000,  1.000000},
        {-1.000000, -0.300000},{ 1.100000, -0.100000},
        {0.500000,  0.700000},{-1.500000, -0.700000}, 
        { 1.100000, -0.800000},{ 1.700000,  1.400000},
        {1.200000,  0.300000},{0.700000, -1.900000},
        {-0.200000, -1.500000},{ 1.800000, -0.600000}

    };
    std::complex<double> G[m*n]={
   {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
   {0.0318 , 0.7952 },   {0.3171 , 0.7547 },
   {0.2769 , 0.1869 },   {0.9502 , 0.2760 },
   {0.0462 , 0.4898 },   {0.0344 , 0.6797 },
   {0.0971 , 0.4456 },   {0.4387 , 0.6551 },
   { 0.8235 , 0.6463},   {0.3816 , 0.1626 }
    };
    std::complex<double> G1[m*n]={
   {0.7060 , 0.7655 },  { 0.6948 , 0.7094 },
   {0.0318 , 0.7952 },   {0.3171 , 0.7547 },
   {0.2769 , 0.1869 },   {0.9502 , 0.2760 },
   {0.0462 , 0.4898 },   {0.0344 , 0.6797 },
   {0.0971 , 0.4456 },   {0.4387 , 0.6551 },
   { 0.8235 , 0.6463},   {0.3816 , 0.1626 }
    };
    Gradexp(X,G,m,n);
    Gradqr(X,G1,m,n);
    std::cout << "Gexp" << std::endl;
    print_matrix(G,m,n);
    std::cout << "Gqr" << std::endl;
    print_matrix(G1,m,n);
    // for (int i =0; i< m*n; ++i)
    // {
    //     std::cout << G[i] << "  ";
    //     if ((i+1)%n == 0)
    //     {
    //         std::cout<< std::endl;
    //     }
    // }
 
   return 0; 
}

void Gradqr(const std::complex<double> *X,std::complex<double> *G,int m,int n)
{
    std::complex<double> alpha1{-0.5,0};
    std::complex<double> alpha{1,0};
    std::complex<double> beta{0,0}; 
    std::complex<double> *temp=new std::complex<double>[n*n];
    cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, n, n, m, &alpha1, X, n, G, n, &beta, temp, n);
    cblas_zgemm(CblasRowMajor, CblasConjTrans, CblasNoTrans, n, n, m, &alpha1, G, n, X, n, &alpha, temp, n);
    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, n, &alpha, X, n, temp, n, &alpha, G, n);
    delete[] temp;
}
void Gradexp(const std::complex<double> *X,std::complex<double> *G, int m,int n)
{
    std::complex<double> alpha{1,0};
    std::complex<double> beta{0,0};
    std::complex<double> alpha1{-1,0};
    std::complex<double> *temp=new std::complex<double>[m*m];
    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasConjTrans, m, m, n, &alpha1, X, n, G, n, &beta, temp, m);
    cblas_zgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, m, n, m, &alpha, temp, m, X, n, &alpha, G, n);
    delete[] temp;
}